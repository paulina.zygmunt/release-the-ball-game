import os

from superwires import games, color
import random

games.init(screen_width = 640, screen_height = 480, fps = 50)

class Magnet(games.Sprite):
    magnet_image = games.load_image("magnet.png")

    def __init__(self):
        super(Magnet, self).__init__(image = Magnet.magnet_image,
                                    x = games.screen.width/6,
                                    y = 300)

        self.score = games.Text(value = 0, size = 25, color = color.white,
                                top = 5, right = games.screen.width - 10)
        games.screen.add(self.score)

    def update(self):
        self.x = games.mouse.x
        self.y = games.mouse.y
        self.check_catch()

        if self.score.value == 100:
            self.end_game()

    def check_catch(self):
        for junk in self.overlapping_sprites:
            self.score.value += 10
            self.score.right = games.screen.width - 10
            junk.handle_caught()

    def end_game(self):
        end_message = games.Message(value="Good job!",
                            size = 100,
                            color = color.green,
                            x = games.screen.width / 2,
                            y = games.screen.height / 2,
                            lifetime = 5 * games.screen.fps,
                            after_death = games.screen.quit)
        games.screen.add(end_message)

class Junk(games.Sprite):
    speed = random.randrange(1, 3)

    def __init__(self, junk_image, x, y=-30):
        super(Junk, self).__init__(image = junk_image,
                                    x = x,
                                    y = y,
                                    dx = random.uniform(-1.2, 2.2),
                                    dy = Junk.speed)
        self.time_til_drop = 0

    def handle_caught(self):
        self.destroy()

class Debris(games.Sprite):
    image = games.load_image("magnet.png")
    debris = os.listdir("space_debris")

    def __init__(self, y = -55, speed = 2, odds_change = 200):
        super(Debris, self).__init__(image = Debris.image,
                                    x = games.screen.width / 2,
                                    y = y,
                                    dx = speed)

        self.odds_change = odds_change
        self.time_til_drop = 0

    def update(self):
        if self.left < 100 or self.right > games.screen.width-100:
            self.dx = -self.dx
        elif random.randrange(self.odds_change) == 0:
           self.dx = -self.dx

        self.check_drop()

    def check_drop(self):
        if self.time_til_drop > 0:
            self.time_til_drop -= 1
        else:
            junk_image = games.load_image("./space_debris/" + random.choice(self.debris))
            new_junk = Junk(junk_image, self.x)
            games.screen.add(new_junk)

            self.time_til_drop = int(new_junk.height * random.randrange(2, 7) / Junk.speed) + 1


def main():
    ground_image = games.load_image("earth.jpg", transparent = False)
    games.screen.background = ground_image

    the_magnet = Magnet()
    games.screen.add(the_magnet)

    games.mouse.is_visible = True
    games.screen.even_grab = True

    the_debris = Debris()
    games.screen.add(the_debris)

    games.screen.mainloop()


main()
